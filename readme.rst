 **E-Commerce**

 This Website is been built on PHP Framework Codeigniter This Website features are : 

- Full Responsive
- Admin Panel 
- Order Tracking
- Complete Validation
- My SQL Database
- Bill Printing Functionality

 

 

**Installation:**
   Goto : 
   application/config/config.php
   
 edit **base_url** to your path 
 
   Upload project.sql file in mysql
   
**Enjoy**


**Quick Access :**
  For accessing Admin features
 Admin id = **admin@admin.com** password : **admin**
 
  For accessing user/Restricted features
 user id = **aman.tyagi@puresoftware.com** password : **aman**

###################
What is CodeIgniter
###################

CodeIgniter is an Application Development Framework - a toolkit - for people
who build web sites using PHP. Its goal is to enable you to develop projects
much faster than you could if you were writing code from scratch, by providing
a rich set of libraries for commonly needed tasks, as well as a simple
interface and logical structure to access these libraries. CodeIgniter lets
you creatively focus on your project by minimizing the amount of code needed
for a given task.
