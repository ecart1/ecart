<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MobiCart</title>

        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/style.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/responsive.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php
        include "include/head_admin.php";
        ?>
        <div class="container">
            <div class="row mt-10"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail cursor-pointer" onclick="location.href = '<?php echo base_url() ?>index.php/user'">
                            <img src="<?php echo base_url() ?>assets/img/icons/user-icon.png" alt="<?php echo ($user_count ? $user_count . ' Users' : 'no user'); ?>">
                            <div class="caption">
                                <h3> <?php echo ($user_count ? $user_count . ' Users' : 'no user'); ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail cursor-pointer" onclick="location.href = '<?php echo base_url() ?>index.php/product'">
                            <img src="<?php echo base_url() ?>assets/img/icons/products-icon.jpg" alt="<?php echo ($product_count ? $product_count . ' Listed Products' : 'no Products'); ?>">
                            <div class="caption">
                                <h3> <?php echo ($product_count ? $product_count . ' Listed Products' : 'no Products'); ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail cursor-pointer" onclick="location.href = '<?php echo base_url() ?>index.php/City'">
                            <img src="<?php echo base_url() ?>assets/img/icons/city.png" alt="<?php echo ($city_count ? $city_count . ' Listed Cities' : 'No City'); ?>">
                            <div class="caption">
                                <h3> <?php echo ($city_count ? $city_count . '  Listed Cities' : 'No City'); ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail cursor-pointer" onclick="location.href = '<?php echo base_url() ?>index.php/product/order'">
                            <img src="<?php echo base_url() ?>assets/img/icons/online-order.png" alt="Total users">
                            <div class="caption">
                                <h3> <?php echo ($order_count ? $order_count . ' Orders' : 'No Orders'); ?></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Latest jQuery form server -->
        <script src="https://code.jquery.com/jquery.min.js"></script>

        <!-- Bootstrap JS form CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <!-- jQuery sticky menu -->
        <script src="<?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.sticky.js"></script>

        <!-- jQuery easing -->
        <script src="<?php echo base_url() ?>assets/js/jquery.easing.1.3.min.js"></script>

        <!-- Main Script -->
        <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    </body>
</html>
