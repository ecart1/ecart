

<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="user-menu">
                    <!--<ul>
                        <li><a href="<?php /*echo base_url() */?>Welcome/login"><i class="fa fa-user"></i> My Account</a></li>
                        <li><a href="#"><i class="fa fa-heart"></i> Wishlist</a></li>
                        <li><a href="<?php /*echo base_url() */?>Welcome/cart"><i class="fa fa-shopping-cart"></i> My Cart</a></li>
                        <li><a href="<?php /*echo base_url() */?>Welcome/checkout"><i class="fa fa-user"></i> Checkout</a></li>

                    </ul>-->
                </div>
            </div>
        </div>
    </div>
</div> <!-- End header area -->

<div class="site-branding-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h1>
                        <a href="<?php echo base_url() ?>index.php/Welcome">
                            <img src="<?php echo base_url()?>/assets/img/site-logo.png" />
                        </a>
                    </h1>
                </div>
            </div>

            <div class="col-sm-6">
                <!--<div class="shopping-item">
                    <a href="cart.php">Cart - <span class="cart-amunt">$800</span> <i class="fa fa-shopping-cart"></i> <span class="product-count">5</span></a>
                </div>-->
            </div>
        </div>
    </div>
</div> <!-- End site branding area -->

<div class="mainmenu-area">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span style="color: white;">menu</span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class=""><a href="<?php echo base_url() ?>index.php/Welcome">Store Front</a></li>
                    <li class=""><a href="<?php echo base_url() ?>index.php/admin">Dashboard</a></li>
                    <li class=""><a href="<?php echo base_url() ?>index.php/product">Products</a></li>
                    <!--<li><a href="<?php echo base_url() ?>index.php/product/add_pro">Add Product</a></li>-->
                    <li><a href="<?php echo base_url() ?>index.php/product/order">Orders</a></li>
                    <li><a href="<?php echo base_url() ?>index.php/City">City</a></li>
                    <li><a href="<?php echo base_url() ?>index.php/User">Users</a></li>
                    <li><a href="<?php echo base_url() ?>index.php/Category">Category</a></li>
            
                </ul>

                <ul class="nav navbar-right navbar-nav">
                    <?php
                    if(isset($_SESSION['email'])) {
                        ?>
                        <li><a class="" href="#">Welcome : <?php echo $_SESSION['dis_name'] ?></a></li>
                        <li><a href="<?php echo base_url() ?>index.php/Login/logout">Log out</a></li>
                        <?php
                    }

                    ?>
                </ul>
            </div>
        </div>
    </div>
</div> <!-- End mainmenu area -->

