<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MobiCart</title>
        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" />
        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/style.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/responsive.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php include "include/head.php"; ?>
        <style>
            .colorgraph {
                height: 5px;
                border-top: 0;
                background: #c4e17f;
                border-radius: 5px;
                background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
                background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            }
        </style>
        <br/>
        <!--Forgot Password Forms-->
        <div style="width:100%;" class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <?php echo form_open('Forgotpassword', 'role="form"'); ?>
                <h2>Forgot password</h2>
                <hr class="colorgraph">
                <p class="flash-error-msg"><?php echo $this->session->flashdata('error'); ?></p>
                <p class="flash-success-msg"><?php echo $this->session->flashdata('success'); ?></p>
                <div class="form-group">
                    <b><?php echo form_error('email'); ?></b>
                    <input type="email" name="email" id="email"  value="<?php echo set_value('email'); ?>" class="form-control input-lg" placeholder="Email Address" tabindex="4">
                    <span class="error-msg" style="display: none;">Enter your email address here</span>
                    <span class="error-msg" id="invalid_email" style="display: none;">Email address is invalid</span>
                </div>
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" value="Submit" id="submit-btn" class="btn btn-primary btn-block btn-lg" tabindex="6" />
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!--end Form-->
        <br/><br/>
        <?php include "include/footer.php"; ?>
        <!-- Latest jQuery form server -->
        <script src="https://code.jquery.com/jquery.min.js"></script>
        <!-- Bootstrap JS form CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <!-- jQuery sticky menu -->
        <script src="<?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.sticky.js"></script>
        <!-- jQuery easing -->
        <script src="<?php echo base_url() ?>assets/js/jquery.easing.1.3.min.js"></script>
        <!-- Main Script -->
        <script src="<?php echo base_url() ?>assets/js/main.js"></script>

        <script>
            jQuery(document).ready(function () {
                jQuery('.error-msg').hide();
                jQuery('#submit-btn').click(function () {
                    var email = jQuery('#email').val();
                    if (email == '') {
                        jQuery('#email').next().show();
                        return false;
                    }
                    if (IsEmail(email) == false) {
                        jQuery('#email').next().hide();
                        jQuery('#invalid_email').show();
                        return false;
                    }
                });
            });
            function IsEmail(email) {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test(email)) {
                    return false;
                } else {
                    return true;
                }
            }
        </script>
    </body>
</html>