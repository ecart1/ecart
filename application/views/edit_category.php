
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MobiCart</title>

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/responsive.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
    
    <script language="JavaScript">
    function cancelEvent(){
        var baseUrl = "<?php echo base_url() ?>index.php/Category";
        window.location= baseUrl;
    }
    </script>
</head>
<body>

<?php
include "include/head_admin.php";
?>
<br/>
<div class="container">
    <div class="well">

        <?php echo form_open_multipart('Category/updateCategory');

        if(isset($suc_mess)){
            echo '<div align="center" class="mainmenu-area"><b>'.$suc_mess. '</b></div>';
        }

        if(!empty($catData)){
        ?>

        <div class="form-group">
            <b><?php echo form_error('category_name'); ?></b>
            <label for="">Category Name:</label>
            <input type="text" value="<?php echo set_value('category_name', $catData['category_name']); ?>" name="category_name" class="form-control" id="category_name">
            <input type="hidden" value="<?php echo (isset($_GET['id'])) ? $_GET['id'] : $catData['cat_id']; ?>" name="cat_id">
        </div>

        <div class="form-group">
            <label for="">Category Description:</label>
            <textarea class="form-control" name="category_des" id="category_des" rows="5"><?php echo set_value('category_des', $catData['category_des']); ?></textarea>
        </div>

        <br /><br />

        <input  type="submit" value="Update" />&nbsp;&nbsp;<input type="button" value="Cancel" class="btnClass" onclick="cancelEvent();" />

    </div>
</div>
<?php
form_close();
?>
<?php
}

?>


<!-- Latest jQuery form server -->
<script src="https://code.jquery.com/jquery.min.js"></script>

<!-- Bootstrap JS form CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- jQuery sticky menu -->
<script src="<?php echo base_url() ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.sticky.js"></script>

<!-- jQuery easing -->
<script src="<?php echo base_url() ?>assets/js/jquery.easing.1.3.min.js"></script>

<!-- Main Script -->
<script src="<?php echo base_url() ?>assets/js/main.js"></script>

</body>
</html>


