<?php

class City extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('city_model');
    }
    public function index()
    {
        $result = $this->city_model->getCity();
        $data['query1'] = $result;
        $this->load->view('city_view', $data);      
    }

    public function cityAdd()
    {
        if (isset($_SESSION['email'])) {
        if ($_SESSION['status'] == 'admin') {
        $cityName= $this->input->post('city_name');
        if($cityName!='')
        {
             $data=array('city_name'=>$cityName,
            'city_status'=>'1');            
            $this->city_model->add($data);
            $this->session->set_flashdata('success', "City Sucessfully Added");
            redirect('City');
   
        }
            $this->load->view('city_add');
         }
        }
        else{
            $this->load->view('index.html');
        }

    }


    function cityDel()
    {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                $id = $_GET['id'];

                $this->city_model->delete($id);

                $this->session->set_flashdata('success', "City Deleted Sucessfully");
                redirect('City');
            }
        }
        else{
            $this->load->view('index.html');
        }
    }

    function cityEdit()
    {

        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {

                $id = $_GET['id'];

                $result = $this->city_model->getCityById($id);

                $data['query1'] = $result;

                $this->load->view('city_edit', $data);
            }
        }
        else{
            $this->load->view('index.html');
        }
    }

    function updateCity()
    {
        if (isset($_SESSION['email'])) {
        if ($_SESSION['status'] == 'admin') {
        $id = $this->input->post('id');    
        $cityName= $this->input->post('city_name');
        
        if($cityName!=''){
        
        $data = array('city_name'=>$cityName);
                $this->city_model->edit($id, $data);

            }
                $this->session->set_flashdata('success', "City Sucessfully Updated");
                redirect('City');

         }
        }
        else{
            $this->load->view('index.html');
        }      
    }
   
}