<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Forgotpassword extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load the Forgotpassword model
//        $this->load->model('User_model');
        $this->load->model('Forgotpassword_model');
    }

    public function index()
    {
        $email = $_POST['email'];
        if (isset($email) && !empty($email))
        {
            $isValidatedEmail = $this->validateEmail($email);
            if ($isValidatedEmail == TRUE)
            {
                $result = $this->Forgotpassword_model->getUserDataByEmail($email);
                if (!empty($result))
                {
                    $user = $result[0];
                    $this->load->helper('string');
                    $password = random_string('alnum', 6);
                    $this->Forgotpassword_model->editUserInfo($user->u_id, array('password' => $password));
                    //Send email
                    $sendEmail = $this->sendEmail($user->email, $password);
                    if ($sendEmail == TRUE)
                    {
                        $this->session->set_flashdata('success', 'Password has been reset and has been sent to email.');
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'Unable to send email.');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'Email address entered do not exist.');
                }
            }
            else
            {
                //add flash data 
                $this->session->set_flashdata('error', 'Invalid Email address.');
            }
        }
        else
        {
            $this->session->set_flashdata('error', 'Please specify a valid email address.');
        }
        redirect('Welcome/Forgotpassword');
//        $this->load->view('forgotpassword');
    }

    public function validateEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function sendEmail($to, $password)
    {
        $this->load->helper('custom_helper');
        $config = smtpConfig();
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('contact@example.com', 'Reset Password - Mobicart');
        $this->email->to($to);
        $this->email->subject('Password reset');
        $this->email->message('<p style="font-size: 16px">You have requested the new password, Here is you new password: <b>' . $password . '</b></p>');
        if ($this->email->send())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}
