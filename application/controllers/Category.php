<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function index() {
        $this->load->model('Category_model');
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                $result = $this->Category_model->getCategory();
                if (empty($result)) {
                    redirect('Category/add_category');
                } else {
                    $data['category'] = $result;
                }
                $data['suc_mess'] = $this->session->flashdata('message');

                $this->load->view('category', $data);
            }
        } else {
            $this->load->view('index.html');
        }
    }

    public function add_category() {
        if (isset($_SESSION['status'])) {
            if ($_SESSION['status'] == 'admin') {
                $this->load->view('add_category');
            }
        } else {
            ?>
            <script>
                alert('Access Denied');
                window.location = "<?php echo base_url() ?>";
            </script>
            <?php
        }
    }

    public function category_upload() {
        $this->load->model('Category_model');
        $this->form_validation->set_rules('category_name', 'Category Name', 'required');
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                $this->load->view('add_category');
            } else {
                    $category_name = $_POST['category_name'];
                    $category_des = $_POST['category_des'];
                    $cat_id = '';
    
                    $result = $this->Category_model->catId();
    
                    foreach ($result as $row) {
                        $cat_id = $row->category_id;
                    }
    
                    $cat_id++;
    
                    $data = array(
                        'category_id' => $cat_id,
                        'category_name' => $category_name,
                        'category_description' => $category_des,
                    );
    
                    $this->Category_model->addCategory($data);
    
                    $this->session->set_flashdata('message', 'Category Sucessfully Added');
    
                    redirect('Category');
                }
            }
        } else {
            $this->load->view('index.html');
        }
    }

    function editCat() {
        $this->load->model('Category_model');
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                $id = $_GET['id'];

                $result = array();
                $resultObj = $this->Category_model->edit_cat($id);

                foreach($resultObj as $tmp )
                {
                    $result['category_name'] = $tmp->category_name;
                    $result['category_des'] = $tmp->category_description;
                }
                $data['catData'] = $result;

                $this->load->view('edit_category', $data);
            }
        } else {
            $this->load->view('index.html');
        }
    }

    function updateCategory() {
        $this->load->model('Category_model');
        $this->form_validation->set_rules('category_name', 'Category Name', 'required');     
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                if ($this->form_validation->run() == FALSE) {
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    $data['catData'] = $this->input->post();
                    $this->load->view('edit_category', $data);
                } else {
                    $category_name = $this->input->post('category_name');
                    $category_des = $this->input->post('category_des');
                    $id = $this->input->post('cat_id');
    
                    $data = array(
                        'category_name' => $category_name,
                        'category_description' => $category_des
                    );
    
                    $this->Category_model->update_category($id, $data);
    
                    $this->session->set_flashdata('message', 'Category Sucessfully Updated');
    
                    redirect('Category');
                }
            }
        } else {
            $this->load->view('index.html');
        }
    }

    function delCat() {
        $this->load->model('Category_model');
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                $id = $this->input->get('id');
                //$id = $_GET['id'];

                $this->Category_model->delete_category($id);

                $this->session->set_flashdata('message', 'Category Sucessfully Deleted');
                redirect('Category');
            }
        } else {
            $this->load->view('index.html');
        }
    }

}
