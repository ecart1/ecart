<?php

class Admin extends CI_Controller
{
    function index(){
        if (!empty($this->session->userdata('email'))) {
            if ($this->session->userdata('status') == 'admin') {
                $users = $this->Admin_model->getUsersCount();
                $data['user_count'] = $users;
                
                $products = $this->Admin_model->getProductCount();
                $data['product_count'] = $products;
                
                $cities = $this->Admin_model->getCityCount();
                $data['city_count'] = $cities;
                
                $orders = $this->Admin_model->getOrdersCount();
                $data['order_count'] = $orders;
                $this->load->view('admin', $data);
            }
        }else {
            redirect('Welcome/login');
        }
    }
}
