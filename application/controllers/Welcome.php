<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function index() {
        $result = $this->Product_model->getProd();

        $data['prod'] = $result;

        $this->load->view('index', $data);
    }

    public function checkout() {
        //$this->load->view('checkout');
        redirect('Checkout');
    }

    public function shop() {
        redirect('Shop');
    }

    public function cart() {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['email'] != '') {
                $this->load->view('cart');
            }
        } else {
            ?>
            <script>
                alert('Access Denied');
                window.location = "<?php echo base_url() ?>";
            </script>
            <?php
        }
    }

    public function single() {
        $id = $_GET['id'];
        redirect('Single?id=' . $id);
    }

    public function login() {
        $this->load->view('login');
    }

    public function register() {
        $this->load->view('register');
    }

    public function forgotpassword()
    {
        $this->load->view('forgotpassword');
    }

}
