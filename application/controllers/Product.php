<?php

class Product extends CI_Controller {

    function index() {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                $prods = $this->Product_model->getProd();
                $data['prods'] = $prods;
                $data['suc_mess'] = $this->session->flashdata('message');

                $this->load->view('product', $data);
            }
        } else {
            $this->load->view('index.html');
        }
    }

    public function do_upload() {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {

                $this->form_validation->set_rules('pro_name', 'Product Name', 'required');
                $this->form_validation->set_rules('pro_cat', 'Product Category', 'required');
                $this->form_validation->set_rules('pro_price', 'Product Price', 'required');

                if ($this->form_validation->run() == FALSE) {
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    $this->load->view('add_pro');
                } else {
                    $config['upload_path'] = './uploads/';
                    $config['allowed_types'] = 'gif|jpg|png|webp';

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('userfile')) {
                        $data['error'] = $this->upload->display_errors();
                        $data['categories'] = $this->Category_model->getCategory();

                        $this->load->view('add_pro', $data);
                    } else {
                        $pro_name = $_POST['pro_name'];
                        $pro_cat = $_POST['pro_cat'];
                        $pro_price = $_POST['pro_price'];
                        $product_des = $_POST['product_des'];
                        $file_name = $this->upload->data('file_name');
                        $file_path = $this->upload->data('file_path');
                        $file_ext = $this->upload->data('file_ext');
                        $p_id = '';

                        $result = $this->Product_model->pID();

                        foreach ($result as $row) {
                            $p_id = $row->p_id;
                        }

                        $p_id++;

                        $data = array(
                            'p_id' => $p_id,
                            'product_name' => $pro_name,
                            'product_category' => $pro_cat,
                            'product_price' => $pro_price,
                            'product_des' => $product_des,
                            'file_name' => $file_name,
                            'file_path' => $file_path,
                            'file_ext' => $file_ext,
                        );

                        $this->Product_model->addProd($data);

                        $this->session->set_flashdata('message', 'Product Sucessfully Added');

                        redirect('Product');
                    }
                }
            }
        } else {
            $this->load->view('index.html');
        }
    }

    function delPro() {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                $id = $_GET['id'];

                $this->Product_model->del_pro($id);

                $this->session->set_flashdata('message', 'Product Sucessfully Deleted');
                redirect('Product');
            }
        } else {
            $this->load->view('index.html');
        }
    }

    function editPro() {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                $id = $_GET['id'];

                $products = array();
                $productsObj = $this->Product_model->edit_pro($id);
                foreach ($productsObj as $tmp) {
                    $products['pro_name'] = $tmp->product_name;
                    $products['pro_cat'] = $tmp->product_category;
                    $products['pro_price'] = $tmp->product_price;
                    $products['product_des'] = $tmp->product_des;
                    $products['file_name'] = $tmp->file_name;
                }
                $data['edPro'] = $products;
                $data['categories'] = $this->Category_model->getCategory();

                $this->load->view('edit_pro', $data);
            }
        } else {
            $this->load->view('index.html');
        }
    }

    function updatePro() {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {

                $this->form_validation->set_rules('pro_name', 'Product Name', 'required');
                $this->form_validation->set_rules('pro_cat', 'Product Category', 'required');
                $this->form_validation->set_rules('pro_price', 'Product Price', 'required');

                if ($this->form_validation->run() == FALSE) {
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    $data['edPro'] = $this->input->post();
                    $data['categories'] = $this->Category_model->getCategory();
                    $this->load->view('edit_pro', $data);
                } else {
                    $config['upload_path'] = './uploads/';
                    $config['allowed_types'] = 'gif|jpg|png';

                    $this->load->library('upload', $config);

                    $pro_name = $_POST['pro_name'];
                    $pro_cat = $_POST['pro_cat'];
                    $pro_price = $_POST['pro_price'];
                    $product_des = $_POST['product_des'];
                    $file_name = $this->upload->data('file_name');
                    $file_path = $this->upload->data('file_path');
                    $file_ext = $this->upload->data('file_ext');
                    $id = $_POST['id'];

                    $data = array(
                        'product_name' => $pro_name,
                        'product_category' => $pro_cat,
                        'product_price' => $pro_price,
                        'product_des' => $product_des
                            //'file_name' => $file_name,
                            //'file_path' => $file_path,
                            //'file_ext' => $file_ext,
                    );

                    $this->Product_model->upd_pro($id, $data);

                    $this->session->set_flashdata('message', 'Product Sucessfully Updated');

                    redirect('product');
                }
            }
        } else {
            $this->load->view('index.html');
        }
    }

    function order() {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                $result = $this->Product_model->getOrders();

                $data['order1'] = $result;

                $this->load->view('orders', $data);
            }
        } else {
            $this->load->view('index.html');
        }
    }

    function changeStatus() {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['status'] == 'admin') {
                $po_id = $_GET['po_id'];

                $this->Product_model->updStatus($po_id, 'delivered');

                redirect('Product/order');
            }
        } else {
            $this->load->view('index.html');
        }
    }

    public function add_pro() {
        if (isset($_SESSION['status'])) {
            if ($_SESSION['status'] == 'admin') {
                $data['categories'] = $this->Category_model->getCategory();
                //echo'<pre>';print_r($data['categories']);
                $this->load->view('add_pro', $data);
            }
        } else {
            ?>
            <script>
                alert('Access Denied');
                window.location = "<?php echo base_url() ?>";
            </script>
            <?php
        }
    }

    public function orders() {
        if (isset($_SESSION['status'])) {
            if ($_SESSION['status'] == 'admin') {
                $this->load->view('orders');
            }
        } else {
            ?>
            <script>
                alert('Access Denied');
                window.location = "<?php echo base_url() ?>";
            </script>
            <?php
        }
    }

    public function reciept() {
        $oh = $_GET['oh'];

        $result = $this->Checkout_model->getOrder($oh);

        $data['order'] = $result;

        $this->load->view('reciept', $data);
    }

}
