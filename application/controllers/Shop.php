<?php

class Shop extends CI_Controller {

    function index() {
        $result = $this->Shop_model->getProd();
        $data['prods'] = $result;
        $this->load->view('shop', $data);
    }

    function catFilter() {
        $cat = $_POST['cat'];
        $result = $this->Shop_model->getProdCat($cat);
        $data['catData'] = $result;
        $this->load->view('shop', $data);
    }

}
