<?php

class Login_test extends TestCase {

    protected $CI;

    function __construct() {
        parent::__construct();
        // Load CI application
        $this->CI = &get_instance();
    }

    public function setUp() {
        $this->resetInstance();
        $this->CI->load->library('session');
    }

    public function testLoginFalse() {
        $credential = [
            'email' => 'user@ad.com',
            'password' => 'incorrectpass'
        ];

        $response = $this->request('POST', ['Login', 'index'], $credential);
        $this->assertContains('Invalid Username or Password', $response);
    }

    public function testLoginTrue() {
        $this->resetInstance();
        $credential = [
            'email' => 'admin@admin.com',
            'password' => 'admin'
        ];

        $this->request('POST', ['Login', 'index'], $credential);
        $response = $this->CI->session->userdata();
        $this->assertArrayHasKey('username', $response);
    }

}
