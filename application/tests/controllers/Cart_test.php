<?php

/**
 * Description of Cart_test
 *
 * @author manoj
 */
class Cart_test extends TestCase {

    public function setUp() {
        parent::setUp();
        $this->request->setCallable(
                function ($CI) {
            $CI->session->email = 'manoj@yop.com';
            $CI->session->u_id = '3';
            $CI->session->dis_name = '';
        }
        );
    }

    public function test_index() {
        $output = $this->request('GET', 'Cart/index', ['id' => 1]);
        //$expected = 'samsung galaxy j7';
        //$this->assertContains($expected, $output);
        $this->assertRedirect('Shop');
    }

    public function test_view_cart() {
        $output = $this->request('GET', 'Cart/viewCart');
        $expected = 'Shoping cart is empty';
        // $this->assertContains($expected, $output);
        //$this->assertNotContains($expected, $output);
        $this->assertRegExp('/samsung galaxy j7/', $output);
    }

//
//    public function test_edit_qty() {
//        
//    }
//
    public function test_del_item() {
        $output = $this->request('GET', 'Cart/delItem', ['id' => 1]);
        
        #$this->assertRegExp('/Product removed successfully/', $output);
        $this->assertRedirect('Cart/viewCart');
    }

}
