<?php

/**
 * Description of Category_test
 *
 * @author manoj
 */
class Category_test extends TestCase {

    public function setUp() {
        parent::setUp();
        $this->request->setCallable(
                function ($CI) {
            $CI->session->email = 'admin@admin.com';
            $CI->session->status = 'admin';
            $CI->session->dis_name = 'admin';
        }
        );
    }

    public function test_index() {

        $output = $this->request('GET', 'category/index');
        $expected = 'Category Name';
        $this->assertContains($expected, $output);
    }

    public function test_add_category() {
        $output = $this->request('GET', 'category/add_category');
        $expected = 'Category Name';
        $this->assertContains($expected, $output, 'Category name string found');
    }

    public function test_category_upload() {
        $data = ['category_name' => 'android',
            'category_des' => 'android  description',
        ];
        $output = $this->request('POST', 'category/category_upload', $data);

        $expected = 'Category Sucessfully Added';
        // $this->assertContains($expected, $output);
        $this->assertRedirect('Category');
    }

    public function test_edit_category() {
        $id = array('id' => 11);
        $output = $this->request('GET', ['Category', 'editCat'], $id);
        $expected = 'Category Name';
        $this->assertContains($expected, $output, 'Category name string not found');
    }

    public function test_update_category() {
        $data = ['category_name' => 'iphone',
            'category_description' => 'iphone  description',
            'category_id' => '21',
        ];
        $expected = 'Category Sucessfully Updated';
        $output = $this->request('POST', ['Category', 'updateCategory'], $data);
        //var_dump($output);
        //$this->assertContains($expected, $output,'Category name string not found');
        $this->assertRedirect('Category');
    }

    public function test_del_cat() {
        $this->assertTrue(true);
    }

}
