<?php

/**
 * Description of User_test
 *
 * @author manoj
 */
class User_test extends TestCase {

    public function test_index() {
        $output = $this->request('GET', ['User', 'index']);
        $expected = 'aman.tyagi@puresoftware.com';
        $expected1 = 'manoj@yopmail.com';
        $this->assertContains($expected, $output);
        $this->assertContains($expected1, $output);
    }
    
    

}
