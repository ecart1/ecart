<?php

/**
 * Description of single_test
 *
 * @author manoj
 */
class single_test extends TestCase {

    public function setUp() {
        parent::setUp();
    }

    public function test_index() {
        $output = $this->request('GET', ['single', 'index'], ['id' => 1]);
        $expected = 'samsung galaxy j7';
        $this->assertContains($expected, $output);
    }

}
