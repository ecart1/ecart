<?php

/**
 * Description of checkout_test
 *
 * @author manoj
 */
class checkout_test extends TestCase {

    public function setUp() {
        parent::setUp();
        $this->request->setCallable(
                function ($CI) {
            $CI->session->email = 'manoj@yop.com';
            $CI->session->u_id = '3';
            $CI->session->dis_name = '';
        }
        );
    }

    public function test_index() {
        $output = $this->request('GET', 'checkout/index', ['tot' => 10000]);
        $expected = 'Your order';

        $this->assertContains($expected, $output);
    }

    public function test_confirm_form() {
        $data = ['fname' => 'manoj',
            'lname' => 'manoj',
            'address' => 'Delhi 91', 'city' => 'Delhi',
            'email' => 'manoj@yopmail.com', 'phone' => '9971522404',
            'netTotal' => '16000' 
        ];
        $output = $this->request('POST', 'checkout/confirmForm', $data);
        
        $this->assertRedirect('Product/reciept');
        //$this->assertNotContains($expected, $output);
    }

}
