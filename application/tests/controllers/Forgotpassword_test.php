<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Forgotpassword_test extends TestCase
{

//    public function test_sendEmail()
//    {
//        $to = 'tanmay.antlabs@gmail.com';
//        $password = 'testPass';
//        $output = $this->request('POST', ['Category', 'postCat', $to, $password]);
//        $this->assertEquals('TRUE', $output);
//    }

    public function emailProvider()
    {
        return [
            ['test_1@gmail.com'],
            ['test_2@yahoo.co.in'],
            ['useless_email@gmail.com']
        ];
    }

    /**
     * @dataProvider emailProvider
     */
    public function test_validateEmail($a)
    {
        $output = $this->request('GET', ['Category', 'validateEmail', $a]);
        $this->assertEquals('TRUE', $output);

        $this->assertNotNull($a);
        $this->assertNotEmpty($a);
        $this->assertRegExp('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix', $a);
    }

    public function test_indexController()
    {
        $output = $this->request('GET', 'Welcome/forgotpassword');
        $this->assertContains('<h2>Forgot password</h2>', $output);
    }

}
