<?php

/**
 * Description of Shop_test
 *
 * @author manoj
 */
class Shop_test extends TestCase {

    public function test_index() {
        $output = $this->request('GET', ['shop', 'index']);
        $expected = 'samsung galaxy j7';
        $expected1 = 'samsung galaxy j1';
        $this->assertContains($expected, $output);
        $this->assertContains($expected1, $output);
    }

    public function test_cat_filter() {
        $output = $this->request('POST', ['shop', 'catFilter'], ['cat' => 1]);
        $expected = 'samsung galaxy j7';
        $expected1 = 'samsung galaxy j1';
        $this->assertContains($expected, $output);
        $this->assertContains($expected1, $output);
    }

}
