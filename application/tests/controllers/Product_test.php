<?php

class Product_test extends TestCase {

    public function setUp() {
        parent::setUp();
        $this->request->setCallable(
                function ($CI) {
            $CI->session->email = 'admin@admin.com';
            $CI->session->status = 'admin';
            $CI->session->dis_name = 'admin';
        }
        );
    }

    function test_index() {
        $output = $this->request('GET', 'product/index');
        $expected = 'Product Name';
        $this->assertContains($expected, $output);
    }

    public function test_do_upload() {
        $data = ['pro_name' => 'android',
            'pro_cat' => '2',
            'pro_price' => '4321',
            'pro_des' => 'android  description',
            'file_name' => 'android-wallpaper-artistic-asphalt-799443.jpg',
            'file_path' => '/var/www/html/ps/ecart/uploads/',
            'file_ext' => '.jpg',
        ];
        $output = $this->request('POST', 'product/do_upload', $data);
        $expected = 'Product Sucessfully Added';
    }

    function test_delPro() {
        $data = ['id' => '3'];
        $output = $this->request('GET', ['Product', 'delPro'], $data);
        $expected = 'Product Sucessfully Deleted';
    }

    function test_editPro() {
        $data = ['pro_name' => 'iphone',
            'pro_price' => '91000',
            'product_des' => 'iphone  description',
            'id' => '2',
        ];
        $output = $this->request('GET', ['Product', 'editPro'], $data);
      //  $this->assertRedirect('Product');
    }

    function test_updatePro() {
        $data = ['pro_name' => 'iphone',
            'pro_price' => '91000',
            'product_des' => 'iphone  description',
            'id' => '2',
            'pro_cat' => '2',
        ];
        $expected = 'Product Sucessfully Updated';
        $output = $this->request('POST', ['Product', 'updatePro'], $data);
        //$this->assertContains($expected, $output, 'Product name string not found');
      //  $this->assertRedirect('Product');
    }

    function test_order() {
        $this->assertTrue(true);
    }

    function test_changeStatus() {
        $id = array('po_id' => 3);
        $output = $this->request('GET', ['Product', 'changeStatus'], $id);
        //$this->assertRedirect('Product/order');
    }


    public function test_orders() {
        $this->assertTrue(true);
    }

    public function test_reciept() {
        $this->assertTrue(true);
    }

}
