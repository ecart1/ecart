<?php

/**
 * Description of Admin_test
 *
 * @author manoj
 */
class Admin_test extends TestCase {

    public function test_index() {
        // This request is redirected to '/auth/login'
        $this->request('GET', 'admin/index');
        $this->assertRedirect('Welcome/login');
    }
    
    

}
