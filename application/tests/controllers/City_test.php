<?php

/**
 * Description of City_test
 *
 * @author manoj
 */
class City_test extends TestCase {

    public function setUp() {
        parent::setUp();
        $this->request->setCallable(
                function ($CI) {
            $CI->session->email = 'admin@admin.com';
            $CI->session->status = 'admin';
            $CI->session->dis_name = 'admin';
        }
        );
    }

    public function test_index() {
        $output = $this->request('GET', ['City', 'index']);
        $expected = 'Noida';
        $expected1 = 'Delhi';
        $this->assertContains($expected, $output);
        $this->assertContains($expected1, $output);
    }

   public function test_add_category_insert() {
        $data = ['city_name' => 'Agra',
             
        ];
        $output = $this->request('POST', 'City/cityAdd', $data);

        $expected = 'City Sucessfully Added';
        // $this->assertContains($expected, $output);
        $this->assertRedirect('City');
    }

    public function test_edit_city() {
        $id = array('id' => 1);
        $output = $this->request('GET', ['City', 'cityEdit'], $id);
        $expected = 'City Name';
        $this->assertContains($expected, $output, 'City name string not found');
    }

    public function test_edit_city_update() {
        $this->assertTrue(true);
    }

    public function test_delete_city() {
         $id = array('id' => 3);
         $output = $this->request('GET', ['City', 'cityDel'], $id); 
         $this->assertRedirect('City');
    }

}
