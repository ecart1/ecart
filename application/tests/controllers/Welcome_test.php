<?php

class Welcome_test extends TestCase
{

    protected $CI;

    function __construct()
    {
        parent::__construct();
        // Load CI application
        $this->CI = &get_instance();
    }

    public function setUp()
    {
        $this->resetInstance();
    }

    public function testLogin()
    {
        $response = $this->request('GET', ['Welcome', 'login']);
        $this->assertRegExp('/Sign In/', $response);
    }

    public function testForgotpassword()
    {
        $response = $this->request('GET', ['Welcome', 'forgotpassword']);
        $this->assertRegExp('/Forgot password/', $response);
    }

    public function testRegister()
    {
        $response = $this->request('GET', ['Welcome', 'register']);
        $this->assertRegExp('/Sign Up/', $response);
    }

}
