<?php

class Admin_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = &get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('Admin_model');
    }

    function test_get_users_Count() {
        $expected = 7;
        $count = $this->obj->getUsersCount();
        $this->assertEquals($expected,$count);
    }

    function test_get_product_count() {
        $expected = 7;
        $count = $this->obj->getProductCount();
        $this->assertEquals($expected,$count);
    }

    function test_get_city_count() {
        $expected = 2;
        $count = $this->obj->getCityCount();
        $this->assertEquals($expected,$count);
    }

    function test_get_order_count() {
        $expected = 5;
        $count = $this->obj->getOrdersCount();
        $this->assertEquals($expected,$count);
    }

}
