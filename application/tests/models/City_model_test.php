<?php

class City_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = &get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('City_model');
        $this->data = ['city_name' => 'Maharastra',
                       'city_status' => 1,
        ];
    }

    function test_add() {
        $result = $this->obj->add($this->data);
        $this->assertTrue($result, 'City Inserted');
    }

    function test_edit() {
        $result = $this->obj->getCityById(7);
        foreach ($result as $category) {
            $this->assertSame($this->data['city_name'], $category->city_name);
        }
    }
    
    function test_update() {
        $this->data['city_name'] = 'Maharastra Edit';
        $result = $this->obj->edit(7, $this->data);
        $this->assertTrue($result, 'City Edited');
    }
    
    function test_fetchAll() {
        $expected = [
            1 => 'Noida',
            2 => 'Delhi',
            3 => 'Gurgaon',
        ];
        $list = $this->obj->getCity();
        foreach ($list as $category) {
            $this->assertSame($expected[$category->idcity_details], $category->city_name);
        }
    }
    
    function test_delete() {
        $expected = 7;
        $result = $this->obj->delete(3);
        $this->assertTrue($result, 'City deleted');
    }

}
