<?php

class Cart_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = &get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('Cart_model');
        $this->data = ['product_name' => 'samsung galaxy j2',
            'product_price' => 15000,
        ];
        $this->newData = ['p_id'=>3,
            'product_name' => 'samsung galaxy j2',
            'product_price' => '15000',
            'file_name' => 'android-android-phone-cell-phone-404280.jpg',
            'quantity' => '1',
            'u_id' => '3',
        ];
    }

    function test_check_pro() {
        $result = $this->obj->check_pro(3,3);
        foreach ($result as $product) {
            $this->assertEquals($this->data['product_name'], $product->product_name);
        }
    }

    function test_find_max() {
        $result = $this->obj->find_max();
        $this->assertTrue(true);
    }

    function test_insPro() {
        $result = $this->obj->insPro($this->newData);
        $this->assertTrue($result);
    }

    function test_upd_pro() {
        $result = $this->obj->upd_pro(2, 1, 1);
        $this->assertTrue($result);
    }

    function test_get_cart() {
        $list = $this->obj->get_cart(3);
        foreach ($list as $product) {
            $this->assertSame('samsung galaxy j7', $product->product_name);
        }
    }

    function test_check_prod() {
        $result = $this->obj->check_prod(3);
        foreach ($result as $product) {
            $this->assertSame('samsung galaxy j7', $product->product_name);
        }
    }

    function test_del_item() {
        //$list = $this->obj->del_item(3,3);
        //$this->assertTrue($result, 'product not deleted');
    }

}
