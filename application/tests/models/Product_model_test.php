<?php

class Product_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = & get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('Product_model');
    }

    function test_p_id() {
        $expected = 6;
        $result = $this->obj->pId($expected);
        $this->assertEquals($expected, $result);
    }

    function test_add_prod() {
        $expected = [
            'product_name' => 'samsung',
            'product_category' => '2',
            'product_price' => '13000'
        ];
        $list = $this->obj->addProd($expected);
    }

    function test_get_prod() {
        $expected = [
            'product_name' => 'samsung galaxy j9'
        ];
        $list = $this->obj->getProd();
        foreach ($list as $product) {
            $this->assertEquals($expected[$product->product_id], $product->product_name);
        }
    }

    function test_del_pro() {
        $expected = [
            'p_id' => '12'
        ];
        $list = $this->obj->del_pro();
        $this->assertTrue(true);
    }

    function test_edit_pro() {
        $expected = [
            'p_id' => '1'
        ];
        $list = $this->obj->edit_pro();
        foreach ($list as $product) {
            $this->assertEquals($expected[$product->product_name], $product->product_price
                    , $product->product_des, $product->product_category);
        }
    }

    function test_upd_pro() {
        $expected = [
            'p_id' => '1',
            'product_name' => 'samsung',
            'product_category' => '2',
            'product_price' => '13000'
        ];
        $list = $this->obj->upd_pro($expected);
        $this->assertTrue(true);
    }

    function test_get_orders() {
        $list = $this->obj->getOrders();
        foreach ($list as $order) {
            $this->assertEquals($expected[$order->bill_fname], $order->bill_city
                    , $order->bill_email, $order->cart_total, $order->bill_phone
                    , $order->status);
        }
    }

    function test_upd_status() {
        $expected = [
            'po_id' => '2',
            'status' => '1'
        ];
        $result = $this->obj->updStatus();
        $this->assertEquals($expected, $result);
    }

}
