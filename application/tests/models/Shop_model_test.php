<?php

class Shop_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = & get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('Shop_model');
        $this->data = ['product_name' => 'samsung galaxy j7',
            'product_price' => 15000,
        ];
    }

    public function test_get_prod() {
        $product = $this->obj->getProd();
         
        $this->assertEquals($this->data['product_name'], $product[0]->product_name);
        
    }

    public function getProdCat() {
        $product = $this->obj->getProdCat(1);
       $this->assertEquals($this->data['product_name'], $product[0]->product_name);
    }

}
