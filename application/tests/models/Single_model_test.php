<?php

class Single_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = & get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('Single_model');
        $this->data = ['product_name' => 'samsung galaxy j7',
            'product_price' => 15000,
        ];
    }

    public function test_get_pro() {
        $product = $this->obj->getPro(1);
        $this->assertEquals($this->data['product_name'], $product[0]->product_name);
    }

    public function test_rel_pro() {
        $product = $this->obj->rel_pro();
        $this->assertEquals($this->data['product_name'], $product[0]->product_name);
    }

}
