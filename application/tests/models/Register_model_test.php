<?php

class Register_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = & get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('Register_model');
        $this->data = ['fname' => 'test',
            'lname' => 'test',
            'username' => 'test123',
            'email' => 'test@test.com',
            'password' => 'test@123',
            'status' => '',
        ];
    }

    public function test_max_id() {
        $result = $this->obj->maxId();
        $this->assertGreaterThan(8, $result[0]->u_id, '');
    }

    public function test_inser_user() {
        $result = $this->obj->insertUser($this->data);
        $this->assertTrue($result);
    }

}
