<?php

class Forgotpassword_model_test extends UnitTestCase
{

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        $CI = & get_instance();
    }

    public function setUp()
    {
        $this->obj = $this->newModel('Forgotpassword_model');
    }

    public function test_getUserDataByEmail()
    {
        $expectedEmail = 'tanmay.antlabs@gmail.com';
        $param_email = 'tanmay.antlabs@gmail.com';
//        $param_email = 'tanmay.mishra@puresoftware.com';
        $user_data = $this->obj->getUserDataByEmail($param_email);
        $userData = json_decode(json_encode($user_data), TRUE);
        foreach ($userData as $user)
        {
            $this->assertSame($expectedEmail, $user['email']);
        }
    }

    public function test_getUser()
    {
        $expectedkeys = ['u_id', 'fname', 'lname', 'username', 'email', 'password', 'status'];
        $users = $this->obj->getUser();
        $userData = json_decode(json_encode($users), TRUE);
        foreach ($expectedkeys as $key)
        {
            foreach ($userData as $user)
            {
                $this->assertArrayHasKey($key, $user);
            }
        }
    }

}
