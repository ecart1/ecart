<?php

class Category_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = &get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('Category_model');
        $this->data = ['category_name' => 'iphone',
            'category_description' => 'iphone  description',
        ];
    }

    public function test_get_category_list() {
        $expected = [
            11 => 'smartphone',
            21 => 'iphone',
            22 => 'iphone',
            23 => 'iphone',
               
        ];
        $list = $this->obj->getCategory();
        foreach ($list as $category) {
            $this->assertEquals($expected[$category->category_id], $category->category_name);
        }
    }

    function test_cat_id() {
        $expected = 31;
        $result = $this->obj->catId();
        $this->assertEquals($expected, $result[0]->category_id);
    }

    function test_add_Category() {
        $result = $this->obj->addCategory($this->data);
        $this->assertTrue($result, 'category Inserted');
    }

    function test_edit_cat() {
        $result = $this->obj->edit_cat(22);
        foreach ($result as $category) {
            $this->assertEquals($this->data['category_name'], $category->category_name);
        }
    }

    function test_update_category() {
        $this->data['category_name'] = 'iphone Edit';
        $result = $this->obj->update_category(21, $this->data);
        $this->assertTrue($result, 'Category Edited');
    }

    function test_delete_category() {
        $result = $this->obj->delete_category(23);
        $this->assertTrue($result, 'Category deleted');
    }

    public function provider() {
        return ['category_name' => 'basic',
            'category_description' => 'basic phone description',
        ];
    }

}
