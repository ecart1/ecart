<?php

class Login_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = &get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('Login_model');
    }

    public function test_check_user() {
           $result = $this->obj->checkUser('manoj@yopmail.com',123);
           $this->assertEquals('manoj', $result->fname);
    }

}
