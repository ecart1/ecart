<?php

class Checkout_model_test extends UnitTestCase {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        $CI = &get_instance();
    }

    public function setUp() {
        $this->obj = $this->newModel('Checkout_model');
        $this->data = array(
            'u_id' => '3',
            'cart_total' => '200',
            'bill_fname' => 'manoj',
            'bill_lname' => 'lumar',
            'bill_add' => 'Noida sector 62',
            'bill_city' => 'Noida',
            'bill_email' => 'manoj.kumar@puresoftware.com',
            'bill_phone' => '9971522404'
        );
    }

    function test_get_cart() {
        $result = $this->obj->getCart(3);
        foreach ($result as $product) {
            $this->assertEquals('samsung galaxy j2', $product->product_name);
        }
    }

    /*function test_max_id() {
        $result = $this->obj->maxId(3, 'cart');
        $this->assertEquals(3,$result);
    }*/

    function test_ins() {
        $result = $this->obj->ins('pending_orders',$this->data);
        $this->assertTrue($result);
    }

    function test_del() {
        $result = $this->obj->del(1);
        $this->assertTrue(true);
    }

    function test_get_order() {
        $result = $this->obj->getOrder(3, 3);
        $this->assertTrue(true);
    }

}
