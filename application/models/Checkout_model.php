<?php
 

class Checkout_model extends CI_Model
{
    function getCart($u_id){
        $query = $this->db->get_where('cart',array('u_id' => $u_id));
        $result = $query->result();
        return $result;
    }

    function maxId($t_id,$t_name){
        $this->db->select_max($t_id, $t_id);
        $query = $this->db->get($t_name);
        $result = $query->result();
        return $result;
    }

    function ins($t_name,$data){
       return $this->db->insert($t_name,$data);
    }

    function del($id){
        $this->db->delete('cart', array('u_id' => $id));
    }

    function getOrder($oh){
        $query = $this->db->get_where('purchase_history',array('oh' => $oh));
        $result = $query->result();
        return $result;
    }
}