<?php 
class City_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
		$this->table_name = 'city_details' ;
    }
	function add($dataArray)
	{
		$ins = $this->db->insert_string($this->table_name,$dataArray);
		return $this->db->query($ins);
	}
	
    function edit ($id, $data) {
        $this->db->where('idcity_details', $id);
        return $this->db->update($this->table_name, $data);
    }
	
    function getCityById($id) {
        $query = $this->db->get_where($this->table_name, array('idcity_details' => $id));
        return $query->result();
    }
	
	function getCity() {
        $query = $this->db->get($this->table_name);
        return $query->result();
    }
	function delete($id)
	{
        $this->db->where('idcity_details', $id);
        return $this->db->delete($this->table_name);
	}


}
?>