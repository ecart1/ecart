<?php

class Forgotpassword_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table_name = 'users';
    }

    function getUser()
    {
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    function getUserDataByEmail($email)
    {
        $query = $this->db->get_where($this->table_name, array('email' => $email));
        return $query->result();
    }

    function editUserInfo($u_id, $data)
    {
        $this->db->where('u_id', $u_id);
        $this->db->update('users', $data);
    }

}

?>