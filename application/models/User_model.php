<?php

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'users';
    }

	function getUser() {
		$this->db->where('status != "admin"');
		$query = $this->db->get($this->table_name);
		return $query->result();
	}

    function getUserDetails($u_id) {
        $query = $this->db->get_where($this->table_name, array('u_id' => $u_id));
        $result = $query->result();
        return $result;
    }

}

?>