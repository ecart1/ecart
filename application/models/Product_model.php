<?php

class Product_model extends CI_Model {
    
    function __construct()
     {
          //inherit the parent constructor
          parent::__construct();
     }

    function pId() {
        $this->db->select_max('p_id', 'p_id');
        $query = $this->db->get('products');
        return $query->result();
    }

    function addProd($data) {
        $this->db->insert('products', $data);
    }

    function getProd() {
        $this->db->select('products.*, category.category_name')
                ->from('products')
                ->join('category', 'products.product_category = category.category_id');
        $query = $this->db->get();
        return $query->result();
    }

    function del_pro($id) {
        $this->db->where('p_id', $id);
        $this->db->delete('products');
    }

    function edit_pro($id) {
        $query = $this->db->get_where('products', array('p_id' => $id));
        return $query->result();
    }

    function upd_pro($id, $data) {
        $this->db->where('p_id', $id);
        $this->db->update('products', $data);
    }

    function getOrders() {
        $query = $this->db->get('pending_orders');
        return $query->result();
    }

    function updStatus($po_id, $value) {
        $this->db->set('status', $value);
        $this->db->where('po_id', $po_id);
        $this->db->update('pending_orders');
    }

}
