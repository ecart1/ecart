<?php

class Category_model extends CI_Model {

    function getCategory() {
        $query = $this->db->get('category');
        return $query->result();
    }

    function catId() {
        $this->db->select_max('category_id', 'category_id');
        $query = $this->db->get('category');
        return $query->result();
    }

    function addCategory($data) {
      return  $this->db->insert('category', $data);
    }

    function edit_cat($id) {
        $query = $this->db->get_where('category', array('category_id' => $id));
        return $query->result();
    }

    function update_category($id, $data) {
        $this->db->where('category_id', $id);
        return $this->db->update('category', $data);
    }

    function delete_category($id) {
        $this->db->where('category_id', $id);
        $this->db->delete('category');
    }

}
