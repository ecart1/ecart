<?php

class Admin_model extends CI_Model {

    function getUsersCount() {
        $this->db->where('status != "admin"');
        $query = $this->db->get('users');
        return $query->num_rows();
    }

    function getProductCount() {
        $query = $this->db->get('products');
        return $query->num_rows();
    }
    
    function getCityCount() {
        $query = $this->db->get('city_details');
        return $query->num_rows();
    }

    function getOrdersCount() {
        $query = $this->db->get('pending_orders');
        return $query->num_rows();
    }


}
