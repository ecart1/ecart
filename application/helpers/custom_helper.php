<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('smtpConfig'))
{

    function smtpConfig()
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => 'adobe.testing@puretesting.com', // change it to yours
            'smtp_pass' => '@d0be$#1234', // change it to yours
            'mailtype' => 'html',
            'newline' => "\r\n",
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        return $config;
    }

    function getCartData($u_id)
    {
        // Get a reference to the controller object
        $CI = get_instance();
        // You may need to load the model if it hasn't been pre-loaded
        $CI->load->model('Cart_model');
        // Call a function of the model
        $result = $CI->Cart_model->get_cart($u_id);
        return $result;
    }

}