/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.7.24-0ubuntu0.18.04.1 : Database - ecart
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ecart` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ecart`;

/*Table structure for table `cart` */

CREATE TABLE `cart` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `cart` */

insert  into `cart`(`c_id`,`p_id`,`product_name`,`product_price`,`file_name`,`quantity`,`u_id`) values 
(2,3,'samsung galaxy j2','15000','android-android-phone-cell-phone-404280.jpg',1,3),
(3,1,'samsung galaxy j7','15000','app-connection-data-892757.jpg',2,1),
(4,3,'samsung galaxy j2','15000','android-android-phone-cell-phone-404280.jpg',1,8),
(5,2,'samsung galaxy j1','10000','android-applications-apps-50614.jpg',3,1),
(6,3,'samsung galaxy j2','15000','android-android-phone-cell-phone-404280.jpg',1,1);

/*Table structure for table `category` */

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL,
  `category_description` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

/*Data for the table `category` */

insert  into `category`(`category_id`,`category_name`,`category_description`,`created_at`,`updated_at`) values 
(11,'smartphone','smartphone','2019-01-24 17:29:10','2019-01-24 17:29:10'),
(21,'iphone Edit','iphone  description','2019-01-25 14:00:40','2019-01-25 14:00:40'),
(22,'iphone','iphone  description','2019-01-25 13:55:31','2019-01-25 13:55:31'),
(24,'iphone','iphone  description','2019-01-25 13:58:39','2019-01-25 13:58:39'),
(25,'iphone','iphone  description','2019-01-25 14:00:40','2019-01-25 14:00:40'),
(26,'iphone','iphone  description','2019-01-25 14:02:18','2019-01-25 14:02:18'),
(27,'iphone','iphone  description','2019-01-25 14:03:53','2019-01-25 14:03:53'),
(28,'iphone','iphone  description','2019-01-25 14:04:40','2019-01-25 14:04:40'),
(29,'iphone','iphone  description','2019-01-25 14:05:00','2019-01-25 14:05:00'),
(30,'iphone','iphone  description','2019-01-25 14:05:22','2019-01-25 14:05:22'),
(31,'iphone','iphone  description','2019-01-25 14:05:53','2019-01-25 14:05:53'),
(32,'iphone','iphone  description','2019-01-25 14:09:42','2019-01-25 14:09:42'),
(33,'android','android  description','2019-01-25 17:16:56','2019-01-25 17:16:56'),
(34,'android','android  description','2019-01-25 17:17:44','2019-01-25 17:17:44'),
(35,'test','test','2019-01-25 17:18:23','2019-01-25 17:18:23'),
(36,'android','android  description','2019-01-25 17:18:52','2019-01-25 17:18:52'),
(37,'android','android  description','2019-01-25 17:46:37','2019-01-25 17:46:37'),
(38,'android','android  description','2019-01-25 17:47:35','2019-01-25 17:47:35'),
(39,'android','android  description','2019-01-25 17:48:19','2019-01-25 17:48:19'),
(40,'android','android  description','2019-01-25 17:48:39','2019-01-25 17:48:39'),
(41,'android','android  description','2019-01-25 17:53:58','2019-01-25 17:53:58'),
(42,'android','android  description','2019-01-25 17:57:32','2019-01-25 17:57:32'),
(43,'android','android  description','2019-01-25 18:01:09','2019-01-25 18:01:09'),
(44,'android','android  description','2019-01-25 18:03:27','2019-01-25 18:03:27'),
(45,'android','android  description','2019-01-25 18:03:55','2019-01-25 18:03:55'),
(46,'android','android  description','2019-01-25 18:04:22','2019-01-25 18:04:22'),
(47,'android','android  description','2019-01-25 18:11:29','2019-01-25 18:11:29');

/*Table structure for table `city_details` */

CREATE TABLE `city_details` (
  `idcity_details` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `city_status` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`idcity_details`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `city_details` */

insert  into `city_details`(`idcity_details`,`city_name`,`created_time`,`updated_time`,`city_status`) values 
(1,'Noida','2019-01-17 00:00:00','2019-01-17 00:00:00','1'),
(2,'Delhi','2019-01-17 00:00:00','2019-01-17 00:00:00','0');

/*Table structure for table `pending_orders` */

CREATE TABLE `pending_orders` (
  `po_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `cart_total` varchar(100) NOT NULL,
  `bill_fname` varchar(100) NOT NULL,
  `bill_lname` varchar(100) NOT NULL,
  `bill_add` varchar(100) NOT NULL,
  `bill_city` varchar(100) NOT NULL,
  `bill_email` varchar(100) NOT NULL,
  `bill_phone` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`po_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `pending_orders` */

insert  into `pending_orders`(`po_id`,`u_id`,`cart_total`,`bill_fname`,`bill_lname`,`bill_add`,`bill_city`,`bill_email`,`bill_phone`,`status`,`date`) values 
(1,1,'23000','sdf dADSA',' ASD Aff ','27 sector 5. kala kuan','que','ashish.kumar1@puresoftware.com','7785656565','delivered','2019-01-17 12:42:23'),
(2,2,'16000','sdf dADSA',' ASD Aff ','27 sector 5. kala kuan','khi','ashish.kumar1@puresoftware.com','7785656565','','2019-01-17 15:20:18'),
(3,3,'16000','dsd','fsdf','sdfsd','khi','as@as.com','dsdgsdg','','2019-01-21 20:12:17'),
(4,8,'16000','demo','demo','sdfsd','khi','admin@example.com','9971522404','','2019-01-24 12:38:15'),
(5,8,'1000','demo','demo','sdfsd','khi','admin@admin.com','9971522404','','2019-01-24 12:39:42');

/*Table structure for table `products` */

CREATE TABLE `products` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) NOT NULL,
  `product_category` int(11) NOT NULL,
  `product_price` varchar(100) NOT NULL,
  `product_des` varchar(1000) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `file_path` varchar(100) NOT NULL,
  `file_ext` varchar(100) NOT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `products` */

insert  into `products`(`p_id`,`product_name`,`product_category`,`product_price`,`product_des`,`file_name`,`file_path`,`file_ext`) values 
(1,'samsung galaxy j7',1,'15000','sdfsdfsdf','app-connection-data-892757.jpg','/var/www/html/ps/ecart/uploads/','.jpg'),
(2,'samsung galaxy j1',1,'10000','smartphone','android-applications-apps-50614.jpg','/var/www/html/ps/ecart/uploads/','.jpg'),
(3,'samsung galaxy j2',1,'15000','smartphone','android-android-phone-cell-phone-404280.jpg','/var/www/html/ps/ecart/uploads/','.jpg'),
(4,'samsung galaxy j3',1,'150007','smartphone','android-blur-blurred-background-1092644.jpg','/var/www/html/ps/ecart/uploads/','.jpg'),
(5,'samsung galaxy j8',1,'12000','smartphone','cellphone-device-display-699122.jpg','/var/www/html/ps/ecart/uploads/','.jpg'),
(6,'samsung galaxy j9',1,'11000','smartphone','android-wallpaper-artistic-asphalt-799443.jpg','/var/www/html/ps/ecart/uploads/','.jpg'),
(7,'redme note3',1,'11500','sdfsdf','pictures-for-kids-drawing-3.gif','/var/www/html/ecart/uploads/','.gif');

/*Table structure for table `purchase_history` */

CREATE TABLE `purchase_history` (
  `ph_id` int(11) NOT NULL AUTO_INCREMENT,
  `oh` int(11) NOT NULL,
  `p_name` varchar(100) NOT NULL,
  `p_price` varchar(100) NOT NULL,
  `p_qty` varchar(100) NOT NULL,
  `u_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ph_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `purchase_history` */

insert  into `purchase_history`(`ph_id`,`oh`,`p_name`,`p_price`,`p_qty`,`u_id`,`date`) values 
(1,1,'samsung galaxy j9','11000','2',1,'2019-01-17 12:38:07'),
(2,2,'samsung galaxy j2','15000','1',2,'2019-01-17 15:20:18'),
(3,3,'samsung galaxy j7','15000','1',3,'2019-01-21 20:12:17'),
(4,4,'samsung galaxy j7','15000','1',8,'2019-01-24 12:38:15');

/*Table structure for table `users` */

CREATE TABLE `users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`u_id`,`fname`,`lname`,`username`,`email`,`password`,`status`) values 
(1,'ashish','kumar','ashish','admin@admin.com','admin','admin'),
(2,'aman','tyagi','aman','aman.tyagi@puresoftware.com','user',''),
(3,'manoj','k','manoj','manoj@yopmail.com','123',''),
(4,'komal','jain','komaljain1@2345','komal.jain1@puresoftware.com','123456',''),
(5,'komal','jain','komaljain1235678','komal.jain1@puresoftware.com','123456',''),
(6,'yoy','yoy','yoyo','yoyo@yopmail.com','yoyo',''),
(7,'komal','jain','yopmail','yopmail@puresoftware.com','123456',''),
(8,'demo','demo','demo','demo@demo.com','demo','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
